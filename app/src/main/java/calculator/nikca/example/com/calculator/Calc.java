package calculator.nikca.example.com.calculator;

import android.content.Context;
import android.widget.Toast;

public class Calc {

    //static Context ctx;

    //Calc(Context ctx) {
    //    this.ctx = ctx;
    //}

    public static String calculate(String text) {
        if (text.indexOf('+') > 0) {
            try {
                int operIndex = text.indexOf('+');
                double firstNumber = Double.parseDouble(text.substring(0, operIndex));
                double secondNumber = Double.parseDouble(text.substring(operIndex + 1, text.length()));
                return String.valueOf(firstNumber + secondNumber);
            } catch (Exception e) {
                //Toast.makeText(ctx, "Вы ввели неправильные данные", Toast.LENGTH_LONG).show();
                return "";
            }
        }
        if (text.indexOf('-') > 0) {
            try {
                int operIndex = text.indexOf('-');
                double firstNumber = Double.parseDouble(text.substring(0, operIndex));
                double secondNumber = Double.parseDouble(text.substring(operIndex + 1, text.length()));

                return String.valueOf(firstNumber - secondNumber);

            } catch (Exception e) {
                //Toast.makeText(ctx, "Вы ввели неправильные данные", Toast.LENGTH_LONG).show();
                return "";
            }
        }

        if (text.indexOf('*') > 0) {
            try {
                int operIndex = text.indexOf('*');
                double firstNumber = Double.parseDouble(text.substring(0, operIndex));
                double secondNumber = Double.parseDouble(text.substring(operIndex + 1, text.length()));
                return String.valueOf(firstNumber * secondNumber);
            } catch (Exception e) {
                //Toast.makeText(ctx, "Вы ввели неправильные данные", Toast.LENGTH_LONG).show();
                return "";
            }
        }

        if (text.indexOf('/') > 0) {
            try {
                int operIndex = text.indexOf('/');
                double firstNumber = Double.parseDouble(text.substring(0, operIndex));
                double secondNumber = Double.parseDouble(text.substring(operIndex + 1, text.length()));
                if (secondNumber == 0) {
                    //Toast.makeText(ctx, "Делить на 0 нельзя", Toast.LENGTH_LONG).show();
                    return "Делить на 0 нельзя";
                }
                return String.valueOf(firstNumber / secondNumber);
            } catch (Exception e) {
                //Toast.makeText(ctx, "Вы ввели неправильные данные", Toast.LENGTH_LONG).show();
                return "";
            }
        }
        return "";
    }
}
